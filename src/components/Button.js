import React from "react";
import '../styles/components/button.css'

export default function Button(props) {

  return (  
        <button    
                  className= {'button ' + props.className}            
                disabled={props.disabled}
                type={props.type}
                onClick={props.handler}
                >
                {props.value || props.children}
               
        </button>       
        );
  }