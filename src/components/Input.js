import React from "react";
import '../styles/components/input.css'

export default function Input(props) {

  return (
        <>
            <label htmlFor = {props.id}>{props.label}</label>
            <input
                    className= 'input'
                    id={props.id} 
                    name={props.name}
                    onChange={props.handler}
                    type={props.type}>
            </input>
            {props.isError && <label>{props.errorMessage}</label>}
        </>
        );
  }