import React from "react";
import '../styles/components/pagination.css'

export default function Pagination(props) {
     
    const buttonList=[];
    console.log(props.number)
    for(let i=1;i<=props.number;i++){
         buttonList.push(i)
    }
        


  return (
      <>
        <ul className='pagination'>
          {buttonList.map(function paginationButtons(elem){
            return <li key={elem} onClick ={props.paginationHandler} id={elem}>{elem}</li>
          })}
        </ul>
       
      </>    
    );
  }