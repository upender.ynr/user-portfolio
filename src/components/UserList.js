import React from "react";
import '../styles/components/userList.css'

export default function UserList(props) {
          

  
  return (
      <>
      <table>
      <thead>
        <tr>
          <th>User Id</th>
          <th>First Name</th>
          <th>Last Name</th>
          <th>Email</th>
        </tr>
        </thead>

    
       
        {props.list.length && props.list.map(function listTable(elem){
         return <tbody>
        <tr key={elem.id} onClick={()=>props.displayDetail(elem.id)}>
          <td>{elem.id}</td>
          <td>{elem.first_name}</td>
          <td>{elem.last_name}</td>
          <td>{elem.email}</td>
        </tr>
        </tbody>
         })}
      </table>

     
      
      </>
           
        );
  }