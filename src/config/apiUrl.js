export const LOGIN_URL = 'https://reqres.in/api/login';
export const USER_LIST = 'https://reqres.in/api/users?page=';
export const USER_DETAIL = 'https://reqres.in/api/users/';
