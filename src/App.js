import React , {useState} from 'react';
import Login from './containers/Login';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Dashboard from './containers/Dashboard';
import EmployeeDetail from './containers/EmployeeDetail'

function App() {

  const [auth, setAuth]=useState(false);
  
  return (
    <>
    <Router>
       <Switch>
              <Route exact path='/' component={()=><Login isAuth={auth} authSetter={(param)=>{setAuth( param )}}/>} />             
              <Route exact path='/user' component={() => <Dashboard isAuth={auth} authSetter={(param)=>{setAuth( param )}}/>}/>      
              <Route exact path='/user/:id' component={()=><EmployeeDetail isAuth={auth} authSetter={(param)=>{setAuth( param )}}/>}  />  
          </Switch>
    </Router>
    </>
  );
}

export default App;
