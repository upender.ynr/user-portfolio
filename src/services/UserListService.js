import React from "react";
import axios from 'axios';
import { USER_LIST } from '../config/apiUrl.js'
 
export default function UserListService(pageNumber) {
    const URL = USER_LIST+pageNumber;
   return axios.get(URL)
      
}

