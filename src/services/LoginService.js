import React from "react";
import axios from 'axios';
import { LOGIN_URL } from '../config/apiUrl.js'
 
export default function LoginService(username,password) {

   return axios.post(LOGIN_URL,{
    "email": username,
    "password": password
})
      
}

