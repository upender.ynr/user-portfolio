import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import Button from "../components/Button";
import UserDetailService from "../services/UserDetailService";
import { useParams} from "react-router";
import UserDetail from "../components/UserDetail";
import "../styles/containers/employeeDetail.css"

export default function EmployeeDetail(props) {


  useEffect(() => {
    if(!props.isAuth) {
            history.push("/");
    }
    //TODO--- better error handling
       if(!userdata)
      UserDetailService(id).then((res)=>setUserdata(res.data.data)).catch((err)=>console.log(err));
    

  });

  function handler() {
    props.authSetter(false)
    history.push("/");
  }


  let history = useHistory();
  const { id } = useParams(); //getting param :id using hooks from react-router
  const [userdata, setUserdata] = useState();
  
  return (
    <>
    <div className="employee">
{userdata &&
     <UserDetail
      avatar={userdata.avatar}
      firstName={userdata.first_name}
      lastName={userdata.last_name}
      email={userdata.email}
     ></UserDetail>}
    </div>
    <Button
    className='logout'
      disabled={false}
      type='button'
      handler={handler}
    >
      Logout
    </Button>
    </>
  );
}