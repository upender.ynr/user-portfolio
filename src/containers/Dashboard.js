import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import Button from "../components/Button";
import UserListService from "../services/UserListService";
import UserList from "../components/UserList";
import Pagination from "../components/Pagination";
import '../styles/containers/dashboard.css'

export default function Dashboard(props) {

  useEffect(() => {
    if(!props.isAuth) {
            history.push("/");
    }
    if(list.length===0) {
      UserListService(1).then((res)=>{
      setNumberOfPages(res.data.total_pages)
      setList(res.data.data)
    }
      ).catch((err)=>console.log(err));
    }

  });
//user login validation
  function handler() {
    props.authSetter(false)
    history.push("/");
  }

  function paginationHandler(event){
    UserListService(event.target.id).then((res)=>{
      setList(res.data.data)
    }).catch((err)=>console.log(err)) // //TODO--- better error handling
  }
//router
  function displayDetail(id){
    history.push('/user/'+id);
  }

  let history = useHistory();
  const [numberOfpages, setNumberOfPages] = useState();
  const [list, setList] = useState([]);
  
  return (
    <>
    <div className="dashboard">
     <UserList list ={list}
     displayDetail={displayDetail}
          ></UserList>
           <Pagination 
           paginationHandler={paginationHandler}
           number={numberOfpages}> 
           </Pagination>
    </div>
    <Button
      className='logout'
      disabled={false}
      type='button'
      handler={handler}
    >
      Logout
    </Button>
    </>
  );
}