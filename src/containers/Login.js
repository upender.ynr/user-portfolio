import React, { useState, useEffect } from "react";
import Input from "../components/Input";
import Button from "../components/Button";
import { useHistory } from "react-router-dom";
import LoginService from "../services/LoginService";
import '../styles/containers/login.css'

export default function Login(props) {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState({isError:false, message:'Invalid Login'});

  useEffect(() => {
    if(props.isAuth) {
        history.push("/user")
    }
  });

  function validateForm() {  
    return username.length > 0 && password.length > 0;
  }

  function handleSubmit(event) {
    event.preventDefault();
    LoginService(username, password).then((res)=>props.authSetter(Boolean(res.data.token))).then(()=>history.push("/user")).catch(function(err){setError({isError:true,message:'User Not found'})});
  }

  let history = useHistory();
  const isDisabled = validateForm();
  return (
      <form onSubmit={handleSubmit} className="login">
        <label className='error'>{ error.isError && error.message}</label>
        <Input
            id='username'
            name='username'
            label='username'
            handler={(e)=>{
              setError({isError:false,message:''})
              setUsername(e.target.value)}}
            type='text'
            isError={false}
        >
        </Input>
        <Input
            id='password'
            name='password'
            label='password'
            handler={(e)=>{
              setError({isError:false,message:''})
              setPassword(e.target.value)}}
            type='password'
            isError={false}
        >
        </Input>
        <Button
            disabled={!isDisabled}
            type='submit'
        >
            {'Login'}
        </Button>
      </form>
  );
}