//Project created for demo of React Skills

1) use npm i to install dependencies. Create-react-app is used to make build.
2) localhost://3000 is used as default port
3) react 16.11 is used and this application is build with hooks
4) Components are divided into two major categories stateless and statefull
5) All stateless components are reusable and are under components folder
6) Statefull components are under container folder
7) This application is made responsive for mobile devices
8) Design principles such as KISS, YAGNI, Single responsibility are used
9) Naming convention are kept as much understandable as possible
10) Lifting state up concept is used
11) Token is set via a callback handler in main app component
12) Unit test cases are not written for this application which is against recommened standard due to lack of time and problem with configuring test cases using hooks
13) CSS libraries like Material-UI or preprocessor could have been used but are not used because of the KISS principle and lack of their need in this small application.
14) Redux is also not used for above reason.
15) Alternate approach to handle login authentication is by passing all component to one HOC.
16) Error handling of only Login api is done and for rest it is just simply printed to the console.
